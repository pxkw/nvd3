## About
Repository to study [nvd3](http://nvd3.org).

See [the official examples](http://nvd3.org/examples/) for its detailed usage.

## Install
npm install
./node_modules/.bin/bower install

