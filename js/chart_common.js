'use strict';

const chartCommon = {
 showChart : function(chart){
    return function(){
      $('.spinner').removeClass('hide');
      d3.selectAll("#chart svg > *").remove();
      d3.select('#chart svg').datum(DATA).call(chart)
        .transition().each('end', function(){
            $('.spinner').addClass('hide');
      });
    };
  } 
};

