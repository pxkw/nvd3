'use strict';

const lineAndBarChart = nv.models.linePlusBarChart()
      .margin({top: 30, right: 60, bottom: 50, left: 70})
      .x(function(d,i){return i;})
      .y(function(d,i){return d[1];});

lineAndBarChart.xAxis.tickFormat(function(d){
  var dx = DATA[0].values[d] && DATA[0].values[d][0] || 0;
  return d3.time.format('%x')(new Date(dx));
});

lineAndBarChart.y1Axis.tickFormat(d3.format(',f'));
lineAndBarChart.y2Axis.tickFormat(function(d){return '$'+d3.format(',f')(d);});
lineAndBarChart.bars.forceY([0]);

lineAndBarChart.show = chartCommon.showChart(lineAndBarChart);

