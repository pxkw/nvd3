'use strict';

const cumulativeLineChart = nv.models.cumulativeLineChart()
      .x(function(d) { return d[0]; })
      .y(function(d) { return d[1]/100; }) // adjusting, 100% is 1.00
      .color(d3.scale.category10().range())
      .useInteractiveGuideline(true);

cumulativeLineChart.xAxis.tickFormat(function(d) {
    return d3.time.format('%x')(new Date(d));
});

cumulativeLineChart.yAxis.tickFormat(d3.format(',.1%'));

cumulativeLineChart.show = chartCommon.showChart(cumulativeLineChart);

