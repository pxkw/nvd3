'use strict';

const stackedAreaChart = nv.models.stackedAreaChart()
       .margin({right: 100})
       .x(function(d) { return d[0]; })
       .y(function(d) { return d[1]; })
       .useInteractiveGuideline(true)
       .rightAlignYAxis(true)
       .showControls(true)
       .clipEdge(true);

stackedAreaChart.xAxis .tickFormat(function(d) { 
  return d3.time.format('%x')(new Date(d));
});

stackedAreaChart.yAxis.tickFormat(d3.format(',.2f'));


stackedAreaChart.show = chartCommon.showChart(stackedAreaChart);

