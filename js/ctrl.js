var chart;

nv.utils.windowResize(function(){
  chart.show();
});

var ctrl = {
  selectLineAndBarChart: function(){
    chart = lineAndBarChart;
    chart.show();
  },
  selectCumulativeLineChart: function(){
    chart = cumulativeLineChart;
    chart.show();
  },
  selectStackedAreaChart: function(){
    chart = stackedAreaChart;
    chart.show();
  },
}

window.onload = ctrl.selectLineAndBarChart;
